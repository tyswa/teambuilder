package me.elevenfourtyeight.teambuilder;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Teams teams;
	Events e = new Events(this);
	public static Teams getTeams() {
	    return teams;
	}
	
	@Override
	public void onEnable() {
		Commands cmd = new Commands();
		Bukkit.getServer().getPluginManager().registerEvents(e, this);
		teams = new Teams(this);
		this.getCommand("tbteam").setExecutor(cmd);
		this.getCommand("teams").setExecutor(cmd);
		loadTeams();
	}
	
	@Override
	public void onDisable() {
		
	}
	
	public void loadTeams() {
		teams.setup();
		teams.savePlayers();
	}
	
}

