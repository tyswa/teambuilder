package me.elevenfourtyeight.teambuilder;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.md_5.bungee.api.ChatColor;

public class Teams {

	private Main plugin;

	public Teams(Main plugin) {
	    this.plugin = plugin;
	}

	
	public FileConfiguration teamscfg;
	public File teamsfile;
	
	public void setup() {
		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		
		teamsfile = new File(plugin.getDataFolder(), "teams.yml");
		
		if(!teamsfile.exists()) {
			try {
				teamsfile.createNewFile();
			} catch(IOException e) {
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED + "COULD NOT CREATE TEAMS.YML FILE");
			}
		}
		
		teamscfg = YamlConfiguration.loadConfiguration(teamsfile);
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "CREATED TEAMS.YML FILE");
	}
	
	public FileConfiguration getPlayers() {
		return teamscfg;
	}
	
	public void savePlayers() {
		try {
			teamscfg.save(teamsfile);
		} catch(IOException e) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED + "COULD NOT SAVE PLAYERS.");
		}
	}
	
	public void reloadPlayers() {
	    teamscfg = YamlConfiguration.loadConfiguration(teamsfile);
	}
	
}