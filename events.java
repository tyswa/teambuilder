public class Events implements Listener {
	
	private final Main plugin;

	public Events(Main plugin) {
	    this.plugin = plugin;
	}

	
	private final Team[] teams = {Team.ASSASSIN, Team.WARRIOR, Team.DEFENDER, Team.NINJA};
	private int currentTeam = 0;

	@EventHandler
	public void on(PlayerJoinEvent e) {
	  Player p = e.getPlayer();
	  String teamName = Main.getTeams().getPlayers().getString(p.getUniqueId().toString());
	  Team team;
	  if (teamName != null) {
	    team = Team.valueOf(teamName.toUpperCase());
	  } else {
	    team = teams[this.currentTeam];
	    if (this.currentTeam >= 3) {
	      this.currentTeam = 0;
	    } else {
	      this.currentTeam++;
	    }
	    
	    Main.getTeams().teamscfg.set("teams." + p.getUniqueId().toString(), team.name());
	    Main.getTeams().savePlayers();
	    Main.getTeams().reloadPlayers();
	  }
	  
	  Commands.teams.put(p.getUniqueId(), team);
	  
	  if(team == Team.ASSASSIN) {
	    this.plugin.getServer().getScheduler().runTaskLaterAsynchronously(this.plugin, () -> p.setPlayerListName(ChatColor.BLUE + "Assassin: " + ChatColor.WHITE + p.getName()), 1L);
	  }

	  if(team == Team.NINJA) {
	    this.plugin.getServer().getScheduler().runTaskLaterAsynchronously(this.plugin, () -> p.setPlayerListName(ChatColor.DARK_GREEN + "Ninja: " + ChatColor.WHITE + p.getName()), 1L);
	  }

	  if(team == Team.WARRIOR) {
	    this.plugin.getServer().getScheduler().runTaskLaterAsynchronously(this.plugin, () -> p.setPlayerListName(ChatColor.DARK_RED + "Warrior: " + ChatColor.WHITE + p.getName()), 1L);
	  }

	  if(team == Team.DEFENDER) {
	    this.plugin.getServer().getScheduler().runTaskLaterAsynchronously(this.plugin, () -> p.setPlayerListName(ChatColor.GOLD + "Defender: " + ChatColor.WHITE + p.getName()), 1L);
	  }
	}
	
	@EventHandler
    public void onHit(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof Player) && !(e.getEntity() instanceof Player)) {
            return;
        }

        Player damager = (Player)  e.getDamager();
        Player victim =  (Player) e.getEntity() ;
        Team pTeam = Commands.teams.get(damager.getUniqueId());
        Team vTeam = Commands.teams.get(victim.getUniqueId());

        if(pTeam == vTeam) {
            damager.sendTitle(ChatColor.DARK_RED + "Don't hit teammates!", "", 15, 15, 15);
			e.setCancelled(true);
        } else {
            victim.sendTitle(ChatColor.DARK_GREEN + "You are being attacked!", "", 15, 15, 15);
        }
    }

}
	