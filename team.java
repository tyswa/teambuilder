package me.elevenfourtyeight.teambuilder;

public enum Team {
	
	NINJA, WARRIOR, ASSASSIN, DEFENDER;
}
